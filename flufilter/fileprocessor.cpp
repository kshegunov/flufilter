#include "fileprocessor.h"

#include <QString>
#include <QStringRef>
#include <QVector>
#include <QRegularExpression>

FileProcessor::~FileProcessor()
{
}

bool FlukaLisProcessor::run(QString & line)
{
    return true;
}

FlukaLisIsotopeProcessor::FlukaLisIsotopeProcessor()
    : parseTable(false)
{
}

bool FlukaLisIsotopeProcessor::run(QString & line)
{
    if (parseTable)  {
        if (line.trimmed().startsWith(QLatin1String("#")))  {
            parseTable = false;
            return true;
        }

        QVector<QStringRef> fields = line.splitRef(QRegularExpression("\\s+"), QString::SkipEmptyParts);
        if (fields.size() != 4)
            return false;

        bool okFirst, okSecond;
        double first = fields[2].toDouble(&okFirst), second = fields[3].toDouble(&okSecond);
        if (!okFirst || !okSecond)
            return false;

        if (qFuzzyIsNull(first) && qFuzzyIsNull(second))
            return false;

        return true;
    }

    if (!line.trimmed().startsWith(QLatin1String("# A/Z")))
        return true;

    parseTable = true;
    return true;
}
