#ifndef FILEPROCESSOR_H
#define FILEPROCESSOR_H

class QString;

class FileProcessor
{
public:
    virtual ~FileProcessor();
    virtual bool run(QString &) = 0;
};

class FlukaLisProcessor : public FileProcessor
{
public:
    bool run(QString &) override;
};

class FlukaLisIsotopeProcessor : public FileProcessor
{
public:
    FlukaLisIsotopeProcessor();

    bool run(QString &) override;

private:
    bool parseTable;
};

#endif // FILEPROCESSOR_H
