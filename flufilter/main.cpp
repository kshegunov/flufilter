#include "controller.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Controller controller(&app);
    Q_UNUSED(controller);

    return QApplication::exec();
}
