#include "controller.h"
#include "fileprocessor.h"

#include <functional>

#include <QMessageBox>
#include <QMetaObject>
#include <QCoreApplication>

#include <QFileInfo>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>

static struct  {
    QString name;
    FileProcessor * (*factory)();
} processors[] = {
    { QStringLiteral("Isotope list"), [] () -> FileProcessor * { return new FlukaLisIsotopeProcessor(); } }
};

Controller::Controller(QObject * parent)
    : QObject(parent)
{
    ui.setupUi(&window);

    QMetaObject::invokeMethod(this, &Controller::initialize, Qt::QueuedConnection);
}

void Controller::initialize()
{
    QObject::connect(ui.actionExit, &QAction::triggered, &window, &QMainWindow::close);
    QObject::connect(ui.inputPathButton, &QToolButton::clicked, this, &Controller::selectInputFile);
    QObject::connect(ui.outputPathButton, &QToolButton::clicked, this, &Controller::selectOutputFile);
    QObject::connect(ui.inputPathEdit, &QLineEdit::textChanged, this, &Controller::validateInputFile);
    QObject::connect(ui.outputPathEdit, &QLineEdit::textChanged, this, &Controller::validateInputFile);
    QObject::connect(ui.processButton, &QPushButton::clicked, this, &Controller::process);

    for (const auto processor : qAsConst(processors))
        ui.inputTypeBox->addItem(processor.name);
    ui.inputTypeBox->setCurrentIndex(0);

    window.show();
}

void Controller::process()
{

    QString inputFilePath = ui.inputPathEdit->text();
    QFileInfo inputFileInfo(inputFilePath);

    QScopedPointer<FileProcessor> processor(createProcessor(ui.inputTypeBox->currentIndex()));

    if (!inputFileInfo.isReadable() || !processor)  {
        QMessageBox::warning(&window, tr("File error ..."), tr("Unknown file type, or file can't be read!"));
        return;
    }

    QFile inputFile(inputFilePath);
    if (!inputFile.open(QFile::Text | QFile::ReadOnly))  {
        QMessageBox::warning(&window, tr("File error ..."), tr("Can't open the input file for reading!"));
        return;
    }

    QFile outputFile(ui.outputPathEdit->text());
    if (!outputFile.open(QFile::Text | QFile::WriteOnly | QFile::Truncate))  {
        QMessageBox::warning(&window, tr("File error ..."), tr("Can't open the output file for writing!"));
        return;
    }

    ui.processButton->setEnabled(false);
    QCoreApplication::processEvents();

    QTextStream in(&inputFile), out(&outputFile);

    // Process the line & write to the output stream
    QString line;
    while (in.readLineInto(&line))  {
        if (processor->run(line))
            out << line << endl;
    }

    ui.processButton->setEnabled(true);
}

void Controller::selectInputFile()
{
    QString inputPath = QFileDialog::getOpenFileName(&window, tr("Select input file ..."));
    if (inputPath.isEmpty())
        return;

    ui.inputPathEdit->setText(inputPath);
}

void Controller::selectOutputFile()
{
    QString inputPath = QFileDialog::getSaveFileName(&window, tr("Select output file ..."));
    if (inputPath.isEmpty())
        return;

    ui.outputPathEdit->setText(inputPath);
}

void Controller::validateInputFile()
{
    ui.processButton->setEnabled(QFileInfo(ui.inputPathEdit->text()).isReadable() && !ui.outputPathEdit->text().isEmpty());
}

FileProcessor * Controller::createProcessor(int index) const
{
    Q_ASSERT(index >= 0);
    return processors[index].factory();
}
