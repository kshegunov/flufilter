#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QMainWindow>

#include "ui_mainwindow.h"

class FileProcessor;
class Controller : public QObject
{
    Q_OBJECT

public:
    explicit Controller(QObject * = nullptr);

private slots:
    void initialize();
    void process();
    void selectInputFile();
    void selectOutputFile();
    void validateInputFile();

private:
    FileProcessor * createProcessor(int) const;

private:
    QMainWindow window;
    Ui::MainWindow ui;
};

#endif // CONTROLLER_H
